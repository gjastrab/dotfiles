set runtimepath+=/usr/local/opt/fzf

" let g:python3_host_prog = '/usr/local/bin/python3'

set laststatus=2
set modelines=5
set ts=2 sts=2 sw=2 expandtab
set inccommand=nosplit
set incsearch
set showmatch
set foldmethod=manual
set foldcolumn=2
set number relativenumber
set mouse=a
"set termguicolors

packadd minpac

call minpac#init()

call minpac#add('k-takata/minpac', {'type': 'opt'})
call minpac#add('romainl/flattened')

call minpac#add('bronson/vim-trailing-whitespace')
call minpac#add('junegunn/vim-easy-align')
call minpac#add('nathanaelkane/vim-indent-guides')
call minpac#add('tomtom/tcomment_vim')

call minpac#add('tpope/vim-endwise')
call minpac#add('tpope/vim-fugitive')
call minpac#add('tpope/vim-projectionist')
"call minpac#add('tpope/vim-rails')
call minpac#add('tpope/vim-repeat')
call minpac#add('tpope/vim-surround')

call minpac#add('junegunn/fzf.vim')
call minpac#add('preservim/nerdtree')

" Languages
call minpac#add('elixir-editors/vim-elixir')
"call minpac#add('hashivim/vim-terraform')
"call minpac#add('ElmCast/elm-vim')

call minpac#add('janko/vim-test')

call minpac#add('neovim/nvim-lsp')

call minpac#add('Shougo/deoplete.nvim', {
  \ 'do': ':UpdateRemotePlugins'})
" "" call minpac#add('autozimu/LanguageClient-neovim', {
" ""   \ 'branch': 'next', 'do': '!bash install.sh'})

" minpac commands:
command! PackUpdate call minpac#update()
command! PackClean call minpac#clean()

let mapleader = ","
colorscheme flattened_dark

"
let g:deoplete#enable_at_startup = 1

"autocmd Filetype elm setlocal et ts=4 sw=4 sts=4


" For NeoVim ---------------------------------------- {{{
" }}}

" For NeoVim 0.5 ------------------------------------ {{{
lua << EOF
vim.cmd('packadd nvim-lsp')
local nvim_lsp = require'nvim_lsp'
nvim_lsp.elixirls.setup{
  cmd = { "/Users/greg/elixir/elixir-ls/current/language_server.sh" };
}
nvim_lsp.tsserver.setup{}
EOF

nnoremap <silent> gd      <cmd>lua vim.lsp.buf.declaration()<CR>
nnoremap <silent> <c-]>   <cmd>lua vim.lsp.buf.definition()<CR>
nnoremap <silent> K       <cmd>lua vim.lsp.buf.hover()<CR>
nnoremap <silent> gD      <cmd>lua vim.lsp.buf.implementation()<CR>
nnoremap <silent> <c-k>   <cmd>lua vim.lsp.buf.signature_help()<CR>
nnoremap <silent> 1gD     <cmd>lua vim.lsp.buf.type_definition()<CR>
nnoremap <silent> gr      <cmd>lua vim.lsp.buf.references()<CR>
nnoremap <silent> g0      <cmd>lua vim.lsp.buf.document_symbol()<CR>
nnoremap <silent> gW      <cmd>lua vim.lsp.buf.workspace_symbol()<CR>
" }}}

" For VimTest --------------------------------------- {{{
nnoremap <Leader>t :TestNearest<CR>
nnoremap <Leader>T :TestFile<CR>
nnoremap <Leader>l :TestLast<CR>

let test#strategy = "neovim"
let test#neovim#term_position = "botright"
" }}}


" " For Language Servers ------------------------------ {{{
" let g:LanguageClient_serverCommands = {
"  \ 'elixir': ['/Users/greg/elixir/elixir-ls/current/language_server.sh'],
"  \ }
" "  \ 'rust': ['rls'],
" "  
" "let g:LanguageClient_loggingLevel='DEBUG'
" "let g:LanguageClient_loggingFile='/Users/greg/_LC.log'
" 
" nnoremap <Leader>; :call LanguageClient_contextMenu()<CR>
" nnoremap <silent> K :call LanguageClient#textDocument_hover()<CR>
" nnoremap <silent> gd :call LanguageClient#textDocument_definition()<CR>
" nnoremap <silent> <F2> :call LanguageClient#textDocument_rename()<CR>
" " }}}

" For FZF ------------------------------------------- {{{
let $FZF_DEFAULT_COMMAND='ag -g ""'
nnoremap <silent> <Leader>f :Files<CR>
" }}}
